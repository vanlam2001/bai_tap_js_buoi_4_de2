/*
Bài 1
Tính ngày tháng năm

input: lấy value của user nhập vào
progress: tính ra ngày tháng năm tiếp theo.
+ trường hợp tháng 2 && tháng 12 và tháng 1 ( trường hợp đặc biệt)
+ output: xuất kết quả hôm qa và ngày mai

*/
const removeWhiteSpace = e => e.replace(/\s/g, ""),
    hasValue = e => "" !== removeWhiteSpace(e),
    isNum = e => !isNaN(Number(removeWhiteSpace(e))),
    isInt = e => (e = Number(removeWhiteSpace(e)), !!Number.isInteger(e)),
    isPositive = e => (e = Number(removeWhiteSpace(e))) >= 0,
    dateIsValid = e => {
        return !!moment(e, "DD.MM.YYYY").isValid()
    },
    validateYear = e => e >= 1920 || (alert("Năm cần lớn hơn 1920"), !1),
    validateMonth = e => e >= 1 && e <= 12;
       


document.getElementById("ngaymai").onclick = function() {
        var e = document.getElementById("day").value,
            t = document.getElementById("month").value,
            n = document.getElementById("year").value,
            a = "";
            i = hasValue(e) && isNum(e) && isPositive(e) && isInt(e) && hasValue(t) && isNum(t) && isPositive(t) && isInt(t) && hasValue(n) && isNum(n) && isPositive(n) && isInt(n) && dateIsValid(`${e}.${t}.${n}`);
        if(i &= validateYear(Number(n))){
            (a = new Date(`${n}-${t}-${e}`)).setDate(a.getDate() + 1), document.getElementById("txtngaymai").innerHTML = a.toLocaleDateString("en-GB")
        }else{
            (document.getElementById("txtngaymai").innerHTML = "", alert("Dữ liệu không hợp lệ"))
        } 
}

document.getElementById("ngayhomqua").onclick = function(){
    var e = document.getElementById("day").value,
        t = document.getElementById("month").value,
        n = document.getElementById("year").value,
        a = "";
        i = hasValue(e) && isNum(e) && isPositive(e) && isInt(e) && hasValue(t) && isNum(t) && isPositive(t) && isInt(t) && hasValue(n) && isNum(n) && isPositive(n) && isInt(n) && dateIsValid(`${e}.${t}.${n}`);
    if(i &= validateYear(Number(n))){
        (a = new Date(`${n}-${t}-${e}`)).setDate(a.getDate() - 1), document.getElementById("txtngayhomqua").innerHTML = a.toLocaleDateString("en-GB")
   }else{
    (document.getElementById("txtngayhomqua").innerHTML = "", alert("Dữ liệu không hợp lệ"))
   }    
}

/*
Bài 2
Tính ngày

input: lấy giá trị người dùng nhập vào
progress: year && month > 0 && month < 13   
          Test  year === namNham thì tháng 2 có 29 ngay 
                year !== namNhan thi thang 2 co 28 ngay
          month 1,3,5,7,8,10,12 co 31 ngay
          month 2,4,6,9,11 co 30 ngay 
output: tháng user nhập có bao nhiêu ngày 
*/
document.getElementById('tinhngay').onclick = function () {
    var thang = document.getElementById('thang').value * 1;
    nam = document.getElementById('nam').value * 1;
    nhuan = '';
    tongsongay = 0;

    if (thang <= 0 || thang > 12 || nam < 1) {
        return document.getElementById("txttinhngay").innerHTML = "", void alert("Dữ liệu không hợp lệ")
    } else {
        if (nam % 4 == 0 && nam % 100 != 0 || nam % 400 == 0) {
            nhuan = true;
        } else {
            nhuan = false;
        }
    }

    switch (thang) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12: {
            tongsongay = 31;
        } break;
        case 2: {
            if (nhuan == true) {
                tongsongay = 29;
            } else {
                tongsongay = 28;
            }
        } break;
        case 4: case 6: case 9: case 11: {
            tongsongay = 30;
        } break;
        default: {
            tongsongay = '0';
        }
    }
    document.getElementById('txttinhngay').innerHTML = `Tháng ${thang}` + ` Năm ${nam}` + `<p> Có: ${tongsongay} ngày</p> `;
}

/*
Bài 3
* Đọc số
 * input: lấy value user
 * progress: lấy giá trị từng hàng: hàng trăm, hàng chục, đơn vị
 * Cách đọc rồi cộng lại với nhau
 * input: nhập vào cách đọc của user
*/


document.getElementById('docso').onclick = function () {
    var bachuso = document.getElementById('3chuso').value * 1;
    hangtram = Math.floor(bachuso / 100),
        hangchuc = Math.floor(bachuso % 100 / 10),
        donvi = bachuso % 100 % 10,
        read_hang_chuc = '',
        read_don_vi = '',
        read_hang_tram = '';

    if (bachuso < 100 || bachuso > 999) {
        return document.getElementById('txtdocso').innerHTML = "", void alert("Dữ liệu không hợp lệ")
    } else {
        switch (hangtram) {
            case 1: {
                read_hang_tram = "Một trăm ";
            } break;
            case 2: {
                read_hang_tram = "Hai trăm";
            } break;
            case 3: {
                read_hang_tram = "Ba trăm";
            } break;
            case 4: {
                read_hang_tram = "Bốn trăm";
            } break;
            case 5: {
                read_hang_tram = "Năm trăm";
            } break;
            case 6: {
                read_hang_tram = "Sáu trăm";
            } break;
            case 7: {
                read_hang_tram = "Bảy trăm";
            } break;
            case 8: {
                read_hang_tram = "Tám trăm";
            } break;
            case 9: {
                read_hang_tram = "Chín trăm";
            } break;
        }
    }

    var read_hang_chuc = 'lẻ';

    if (hangchuc % 10 == 0 && donvi != 0) {
    } else {
        switch (hangchuc) {
            case 1: {
                read_hang_chuc = "mười";
            } break;
            case 2: {
                read_hang_chuc = "hai mươi ";
            } break;
            case 3: {
                read_hang_chuc = "Ba mươi ";
            } break;
            case 4: {
                read_hang_chuc = "Bốn mươi ";
            } break;
            case 5: {
                read_hang_chuc = "Năm mươi ";
            } break;
            case 6: {
                read_hang_chuc = "Sáu mươi ";
            } break;
            case 7: {
                read_hang_chuc = "Bảy mươi ";
            } break;
            case 8: {
                read_hang_chuc = "Tám mươi ";
            } break;
            case 9: {
                read_hang_chuc = "Chín mươi ";
            } break;

        }
    }
    switch (donvi) {
        case 1: {
            read_don_vi = "một";
        } break;
        case 2: {
            read_don_vi = "hai";
        } break;
        case 3: {
            read_don_vi = "Ba";
        } break;
        case 4: {
            read_don_vi = "Bốn";
        } break;
        case 5: {
            read_don_vi = "Năm";
        } break;
        case 6: {
            read_don_vi = "Sáu";
        } break;
        case 7: {
            read_don_vi = "Bảy";
        } break;
        case 8: {
            read_don_vi = "Tám";
        } break;
        case 9: {
            read_don_vi = "Chín";
        } break;
    }
    document.getElementById('txtdocso').innerHTML = `${read_hang_tram} ${read_hang_chuc} ${read_don_vi}`
}

/*
Bài 4
Tìm sinh viên xa trường nhất
* tạo giá trị tọa độ của 3 sinh viên và trường học nhập vào
 * progress: dùng công thức tính ra độ dài của từng sinh viên (Math.sqrt) căn bậc
 * so sánh độ dài của từng sinh viên => sinh viên xa trường Nhất
 * output: Xuất ra name của Sv xa trường nhất
*/

document.getElementById('search').onclick = function () {
    var sinhvien1 = document.getElementById('sinhvien1').value;
    sinhvien2 = document.getElementById('sinhvien2').value;
    sinhvien3 = document.getElementById('sinhvien3').value;
    x1 = document.getElementById('x1').value * 1;
    x2 = document.getElementById('x2').value * 1;
    x3 = document.getElementById('x3').value * 1;
    x4 = document.getElementById('x4').value * 1;
    y1 = document.getElementById('y1').value * 1;
    y2 = document.getElementById('y2').value * 1;
    y3 = document.getElementById('y3').value * 1;
    y4 = document.getElementById('y4').value * 1;

    let khoangcach1;
    let khoangcach2;
    let khoangcach3;

    khoangcach3 = Math.sqrt((x4 - x3) * (x4 - x3) + (y4 - y3) * (y4 - y3));
    khoangcach1 = Math.sqrt((x4 - x1) * (x4 - x1) + (y4 - y1) * (y4 - y1));
    khoangcach2 = Math.sqrt((x4 - x2) * (x4 - x2) + (y4 - y2) * (y4 - y2));


    if (x1 > 0 && x2 > 0 && x3 > 0 && x4 > 0 && y1 > 0 && y2 > 0 && y3 > 0 && y4 > 0) {
        test = true;
    } else {
        alert('Tọa độ bạn nhập không đúng');
    }




    if (khoangcach1 === khoangcach2 === khoangcach3) {
        document.getElementById('txtsearch').innerHTML = `Khoảng đường của 3 sinh viên ngang nhau`
    }

    else {
        if (khoangcach1 > khoangcach2 && khoangcach1 > khoangcach3) {
            document.getElementById('txtsearch').innerHTML = `Sinh viên ${sinhvien1} xa nhất`
        } else if (khoangcach2 > khoangcach1 && khoangcach2 > khoangcach3) {
            document.getElementById('txtsearch').innerHTML = `Sinh viên ${sinhvien2} xa nhất`
        } else {
            document.getElementById('txtsearch').innerHTML = `Sinh viên ${sinhvien3} xa nhất`
        }
    }

}